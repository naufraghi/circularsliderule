package it.slug.circularsliderule;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;


public class CircularSlideRuleView extends View {

	private final int internal_color;
	private final int external_color;

	private static final int INVALID_POINTER_ID = -1;
	private static final int M = 10;
	private static final int N = 90;
	private int SIZE; // = 100;
	private int STEP; // = 10;

	private float mPosX;
	private float mPosY;
	private float rotation = 0;
	private boolean rate_rule = false;

	public synchronized float getRulerRotation() {
		return rotation;
	}

	public synchronized void setRulerRotation(float rotation) {
		this.rotation = rotation;
		invalidate();
	}

	public synchronized boolean getRateRule() {
		return rate_rule;
	}

	public synchronized void setRateRule(boolean rate_rule) {
		this.rate_rule = rate_rule;
		invalidate();
	}

	private float mLastTouchX;
	private float mLastTouchY;
	private double startAngle;
	private int mActivePointerId = INVALID_POINTER_ID;

	private final ScaleGestureDetector mScaleDetector;
	private float mScaleFactor = 1.f;

	public CircularSlideRuleView(Context context) {
		this(context, null, 0);
	}

	public CircularSlideRuleView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CircularSlideRuleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircularSlideRuleView, defStyle, 0);
		internal_color = a.getColorStateList(R.styleable.CircularSlideRuleView_internalRulerColor).getDefaultColor();
		external_color = a.getColorStateList(R.styleable.CircularSlideRuleView_externalRulerColor).getDefaultColor();
		a.recycle();
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		// Let the ScaleGestureDetector inspect all events.
		mScaleDetector.onTouchEvent(ev);

		final int action = ev.getAction();
		switch (action & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN: {
			final float x = ev.getX();
			final float y = ev.getY();

			mLastTouchX = x;
			mLastTouchY = y;
			startAngle = getAngle(x, y);
			mActivePointerId = ev.getPointerId(0);
			break;
		}

		case MotionEvent.ACTION_MOVE: {
			final int pointerIndex = ev.findPointerIndex(mActivePointerId);
			final float x = ev.getX(pointerIndex);
			final float y = ev.getY(pointerIndex);

			// Only move if the ScaleGestureDetector isn't processing a gesture.
			if (!mScaleDetector.isInProgress()) {
				final float dx = x - mLastTouchX;
				final float dy = y - mLastTouchY;

				mPosX += dx;
				mPosY += dy;

				double currentAngle = getAngle(x, y);

				rotation += (float) (startAngle - currentAngle);
				startAngle = currentAngle;

				invalidate();
			}

			mLastTouchX = x;
			mLastTouchY = y;

			break;
		}

		case MotionEvent.ACTION_UP: {
			mActivePointerId = INVALID_POINTER_ID;
			break;
		}

		case MotionEvent.ACTION_CANCEL: {
			mActivePointerId = INVALID_POINTER_ID;
			break;
		}

		case MotionEvent.ACTION_POINTER_UP: {
			final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
			final int pointerId = ev.getPointerId(pointerIndex);
			if (pointerId == mActivePointerId) {
				// This was our active pointer going up. Choose a new
				// active pointer and adjust accordingly.
				final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
				mLastTouchX = ev.getX(newPointerIndex);
				mLastTouchY = ev.getY(newPointerIndex);
				mActivePointerId = ev.getPointerId(newPointerIndex);
			}
			break;
		}
		}

		return true;
	}

	/**
	 * @return The angle of the unit circle with the image view's center
	 */
	private double getAngle(double xTouch, double yTouch) {
		double x = xTouch - (this.getWidth() / 2d);
		double y = this.getHeight() - yTouch - (this.getHeight() / 2d);
		switch (getQuadrant(x, y)) {
		case 1:
			return Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI;
		case 2:
			return 180 - Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI;
		case 3:
			return 180 + (-1 * Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI);
		case 4:
			return 360 + Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI;
		default:
			return 0;
		}
	}

	/**
	 * @return The selected quadrant.
	 */
	private static int getQuadrant(double x, double y) {
		if (x >= 0) {
			return y >= 0 ? 1 : 4;
		} else {
			return y >= 0 ? 2 : 3;
		}
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.translate(canvas.getWidth() / 2, (int) (canvas.getHeight() / 2.2));
		float min_side = Math.min(canvas.getWidth(), canvas.getHeight());
		final int SIDE = (int) min_side;
		SIZE = (int) (SIDE / 2.8);
		STEP = SIZE / 10;

		class TicPainter {
			final float font_size = 12 * SIDE / 300;
			public float lognorm(int c) {
				return (float) ((Math.log(c) - Math.log(M)) / (Math.log(N + M) - Math.log(M)));
			}

			public boolean important(int c) {
				if (c < 50)
					return c % 5 == 0;
				if (c < 100)
					return c % 10 == 0;
				return c % 50 == 0;
			}

			public boolean important_hours(int c) {
				if (c > 60)
					return c % 10 == 0;
				if (c <= 12)
					return (c*10) % 10 == 0;
				if (c <= 30)
					return (c*10) % 30 == 0;
				return (c*10) % 60 == 0;
			}

			public void drawRuler(Canvas canvas, boolean internal, boolean rate_rule) {
				Paint pen = new Paint(Paint.ANTI_ALIAS_FLAG);
				pen.setTextSize(font_size);
				float font_height = pen.getFontMetrics().top;
				pen.setStrokeWidth(1);
				pen.setTextAlign(Paint.Align.CENTER);
				for (int c = M; c < N + M; c++) {
					float ratio = (float) 0.8;
					pen.setTextSize(font_size * ratio);
					float r = lognorm(c);
					canvas.save();
					canvas.rotate(r * 360);
					int start, stop;
					if (internal) {
						if (rate_rule) {
							start = -(SIZE-STEP);
							stop = -SIZE;
							if (important_hours(c)) {
								if (important_hours(c)) {
									start = -(SIZE-STEP-STEP/2);
									if (important_hours(c)) {
										start = -(SIZE-2*STEP);
										ratio = 0.8f;
										if (c == 60) {
											ratio *= 1.5;
										}
										pen.setStrokeWidth(2);
										pen.setTextSize(font_size * ratio);
									}
								}
								if (c == 60) {
									pen.setColor(external_color);
								} else {
									pen.setColor(internal_color);
								}
								if (c == 60) {
									canvas.drawText("" + c, 0, start - (2 + font_height) * ratio, pen);
								} else if (c > 60) {
									int hours = c / 60;
									int minutes = c % 60;
									canvas.drawText(String.format("%d:%02d", hours, minutes), 0, start - (2 + font_height) * ratio, pen);
								} else {
									int hours = (c*10) / 60;
									int minutes = (c*10) % 60;
									canvas.drawText(String.format("%d:%02d", hours, minutes), 0, start - (2 + font_height) * ratio, pen);
								}
							} else {
								pen.setStrokeWidth(1);
							}
							pen.setColor(internal_color);
						} else {
							start = -(SIZE-STEP);
							stop = -SIZE;
							if (c < 20 || important(c) || (c > 50 && c % 5 == 0)) {
								if (important(c) || (c > 50 && c % 5 == 0)) {
									start = -(SIZE-STEP-STEP/2);
									if (important(c)) {
										start = -(SIZE-2*STEP);
										ratio = 1;
										if (c == 10) {
											ratio *= 1.5;
										}
										pen.setStrokeWidth(2);
										pen.setTextSize(font_size * ratio);
									}
								}
								if (c == 10) {
									pen.setColor(external_color);
								} else {
									pen.setColor(internal_color);
								}
								canvas.drawText("" + c, 0, start - (2 + font_height) * ratio, pen);
							} else {
								pen.setStrokeWidth(1);
							}
							pen.setColor(internal_color);
						}
					} else {
						start = -SIZE;
						stop = -(SIZE+STEP);
						if (c < 25 || important(c) || (c > 50 && c % 5 == 0)) {
							if (important(c) || (c > 50 && c % 5 == 0)) {
								stop = -(SIZE+STEP+STEP/2);
								if (important(c)) {
									stop = -(SIZE+2*STEP);
									ratio = 1;
									if (c == 10) {
										ratio *= 1.5;
									}
									pen.setStrokeWidth(2);
									pen.setTextSize(font_size * ratio);
								}
							}
							if (c == 10) {
								pen.setColor(internal_color);
							} else {
								pen.setColor(external_color);
							}
							canvas.drawText("" + c, 0, stop - 2 * ratio, pen);
						} else {
							pen.setStrokeWidth(1);
						}
						pen.setColor(external_color);
					}
					canvas.drawLine(0, start, 0, stop, pen);
					canvas.restore();
				}
			}
		}
		canvas.save();
		if (BuildConfig.DEBUG)
			Log.d("DEBUG", "X: " + mPosX + " Y: " + mPosY + " rot: " + rotation);
		TicPainter tics = new TicPainter();
		if (rate_rule)
			canvas.rotate(-tics.lognorm(60) * 360);
		tics.drawRuler(canvas, true, rate_rule);
		canvas.restore();
		canvas.rotate(rotation);
		tics.drawRuler(canvas, false, false);
		canvas.restore();
	}

	private class ScaleListener extends
			ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			mScaleFactor *= detector.getScaleFactor();

			// Don't let the object get too small or too large.
			mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));

			invalidate();
			return true;
		}
	}

}
