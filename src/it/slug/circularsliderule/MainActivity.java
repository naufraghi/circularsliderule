package it.slug.circularsliderule;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

public class MainActivity extends Activity {

	private SharedPreferences prefs;
	private CircularSlideRuleView ruler;
	private int current_theme;
	private boolean rate_rule;
	public final static int LIGHT_THEME = 0;
	public final static int DARK_THEME = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		prefs = getPreferences(0);
		current_theme = prefs.getInt("theme", LIGHT_THEME);
		rate_rule = prefs.getBoolean("rate_rule", false);
		if (current_theme == DARK_THEME) {
			setTheme(R.style.RulerThemeDark);
		}
		super.onCreate(savedInstanceState); // after setTheme...
		setContentView(R.layout.activity_main);
		ruler = (CircularSlideRuleView) findViewById(R.id.view);
		ruler.setRulerRotation(prefs.getFloat("rotation", 0));
	}
	protected void onResume() {
		super.onResume();
		ruler.setRulerRotation(prefs.getFloat("rotation", 0));
	}
	protected void onPause() {
		super.onPause();
		SharedPreferences.Editor ed = prefs.edit();
		ed.putFloat("rotation", ruler.getRulerRotation());
		ed.commit();
	}
	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		Window window = getWindow();
		window.setFormat(PixelFormat.RGB_565);
	}

    @TargetApi(11)
	private void applyNewTheme(){
		if (Build.VERSION.SDK_INT >= 11) {
			recreate();
		} else {
			Intent intent = getIntent();
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			finish();
			overridePendingTransition(0, 0);

			startActivity(intent);
			overridePendingTransition(0, 0);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		SharedPreferences.Editor ed = prefs.edit();
		switch (item.getItemId()) {
			case R.id.menu_toggle_theme:
				if (current_theme == DARK_THEME) {
					current_theme = LIGHT_THEME;
				} else {
					current_theme = DARK_THEME;
				}
				ed.putInt("theme", current_theme);
				ed.commit();
				applyNewTheme();
				return true;
			case R.id.menu_toggle_rate_rule:
				rate_rule = !rate_rule;
				ed.putBoolean("rate_rule", rate_rule);
				ed.commit();
				ruler.setRateRule(rate_rule);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
